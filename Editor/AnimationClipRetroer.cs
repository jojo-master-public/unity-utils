using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Dioinecail.UnityUtils.Animation
{
    public static class AnimationClipRetroer
    {
        [MenuItem("Utility/Animation/Remove Every Second Keyframe")]
        public static void RemoveEverySecondKeyframe()
        {
            var clip = Selection.activeObject as AnimationClip;

            if (clip == null)
                return;

            Undo.RecordObject(clip, "Remove Every Second Keyframe");

            var bindings = AnimationUtility.GetCurveBindings(clip);

            var curves = new List<AnimationCurve>();

            foreach (var b in bindings)
            {
                var curve = AnimationUtility.GetEditorCurve(clip, b);
                var keyframes = curve.keys;

                var trimmedKeyframes = new List<Keyframe>();

                for (int i = 0; i < keyframes.Length; i += 2)
                {
                    int index = i;
                    trimmedKeyframes.Add(keyframes[index]);
                }

                keyframes = trimmedKeyframes.ToArray();

                curve.keys = keyframes;

                curves.Add(curve);
            }

            AnimationUtility.SetEditorCurves(clip, bindings, curves.ToArray());
        }
    }
}