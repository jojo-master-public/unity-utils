# Unity Utils

A repository for handy unity utility scripts

## Installation

Open Unity Package Manager  
Click on a "+" sign in top left corner  
Choose "Add package from git url.."  
Copy and paste this repository url https://gitlab.com/jojo-master-public/unity-utils.git  
Hit Add  
Enjoy!  

## Contact

Developed by Andrew Dioinecail.  
Twitter: [Dioinecail](https://twitter.com/dioinecail).  
Email: andrewdionecail@gmail.com  
  
Feel free to ask questions!  
